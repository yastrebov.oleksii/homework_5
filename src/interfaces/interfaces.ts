import { CourseType, Faculty } from '../enums/enums';

interface Person {
   name: string;
   age: number;
}

interface Course {
   id: string;
   name: string;
   type: CourseType;
   faculty: Faculty;
   capacity: number;
}

export { Person, Course };
