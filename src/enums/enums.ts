enum Faculty {
   Engineering = 'ENGINEERING',
   Science = 'SCIENCE',
   Arts = 'ARTS',
   Business = 'BUSINESS',
}

enum CourseType {
   Lecture = 'LECTURE',
   Lab = 'LAB',
   Seminar = 'SEMINAR',
}

enum Role {
   Janitor = 'JANITOR',
   Accountant = 'ACCOUNTANT',
   Security = 'SECURITY',
   Assistant = 'ASSISTANT',
}

export {Faculty, CourseType, Role}