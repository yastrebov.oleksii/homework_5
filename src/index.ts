import { University } from './classes/university'
import { Student, Professor, QualityControl } from './types/types';
import { StaffMember } from './classes/universityMembers';
import { Course } from './interfaces/interfaces';
import { Department } from './classes/department';
import { CourseType, Faculty, Role } from './enums/enums';

const university = new University('Example University');

const engineeringDepartment = new Department(Faculty.Engineering);
const scienceDepartment = new Department(Faculty.Science);
university.addDepartment(engineeringDepartment);
university.addDepartment(scienceDepartment);

const course1: Course = {
   id: 'course1',
   name: 'Introduction to Programming',
   type: CourseType.Lecture,
   faculty: Faculty.Engineering,
   capacity: 30,
};

const course2: Course = {
   id: 'course2',
   name: 'Data Structures',
   type: CourseType.Lecture,
   faculty: Faculty.Engineering,
   capacity: 30,
};

university.addCourse(course1);
university.addCourse(course2);

university.assignCourseToDepartment('course1', Faculty.Engineering);
university.assignCourseToDepartment('course2', Faculty.Engineering);

console.log(
   'Courses in Engineering department:',
   university.listCoursesByDepartment(Faculty.Engineering)
);

university.addDepartment(engineeringDepartment);
university.addDepartment(scienceDepartment);

university.addCourse(course1);
university.addCourse(course2);

const student1: Student = {
   name: 'Alex',
   age: 20,
   studentId: 'student1',
   courses: [],
};

const student2: Student = {
   name: 'John',
   age: 22,
   studentId: 'student2',
   courses: [],
};

university.addStudent(student1);
university.addStudent(student2);

// Create professors
const professor1: Professor = {
   name: 'Professor Smith',
   age: 45,
   faculty: Faculty.Engineering,
   courses: [],
};

const professor2: Professor = {
   name: 'Professor Johnson',
   age: 50,
   faculty: Faculty.Science,
   courses: [],
};

university.addProfessor(professor1);
university.addProfessor(professor2);

// Create staff members
const staff1: StaffMember = new StaffMember(
   'Mary',
   35,
   Role.Assistant,
   Faculty.Engineering
);

const staff2: StaffMember = new StaffMember(
   'John',
   40,
   Role.Accountant,
   Faculty.Science
);

university.addStaffMember(staff1);
university.addStaffMember(staff2);

// Create QualityControls
const qualityControl1: QualityControl = {
   name: 'Mr. Brown',
   age: 45,
   title: 'Condition checker',
   departments: [Faculty.Engineering],
};

const qualityControl2: QualityControl = {
   name: 'Ms. Green',
   age: 60,
   title: 'Education checker',
   departments: [Faculty.Science],
};

university.addQualityControl(qualityControl1);
university.addQualityControl(qualityControl2);

university.assignProfessorToCourse('Professor Smith', 'course1');
university.assignProfessorToCourse('Professor Johnson', 'course2');

university.enrollStudentInCourse('student1', 'course1');
university.enrollStudentInCourse('student2', 'course2');

console.log(
   'Students in course course1:',
   university.listStudentsInCourse('course1')
);

console.log(
   'Courses taught by Professor Smith:',
   university.listCoursesByProfessor('Professor Smith')
);

console.log(
   'Staff members in Engineering department:',
   university.listStaffMembersByDepartment(Faculty.Engineering)
);

console.log(
   'Courses in Engineering department:',
   university.listCoursesByDepartment(Faculty.Engineering)
);
