import { Person, Course } from "../interfaces/interfaces";
import { Faculty } from "../enums/enums";

type Student = Person & { studentId: string; courses: Course[] };
type Professor = Person & { faculty: Faculty; courses: Course[] };
type Staff = Person & { Role: string; department: string };
type QualityControl = Person & { title: string; departments: string[] };
type ReadonlyCourse = Readonly<Course>;

export { Student, Professor, Staff, QualityControl, ReadonlyCourse };
