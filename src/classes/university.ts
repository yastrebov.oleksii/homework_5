import { Student, Professor } from "../types/types";
import { StaffMember } from "./universityMembers";
import { Course } from "../interfaces/interfaces";
import { QualityControl } from "../types/types";
import { Department } from "./department";
import { Faculty } from "../enums/enums";
import { Log, LogGetter, LogClass } from "../decorators/decorators";

// University Class
@LogClass
class University {
   private _students: Student[] = [];
   private _professors: Professor[] = [];
   private _courses: Course[] = [];
   private _staffMembers: StaffMember[] = [];
   private _QualityControls: QualityControl[] = [];
   private _departments: Department[] = [];

   constructor(public name: string) {}

   @LogGetter
   get students(): Student[] {
      return this._students;
   }

   @LogGetter
   get professors(): Professor[] {
      return this._professors;
   }

   @LogGetter
   get courses(): Course[] {
      return this._courses;
   }

   @LogGetter
   get staffMembers(): StaffMember[] {
      return this._staffMembers;
   }

   @LogGetter
   get QualityControls(): QualityControl[] {
      return this._QualityControls;
   }

   @LogGetter
   get departments(): Department[] {
      return this._departments;
   }

   @Log('Student added')
   addStudent(student: Student): void {
      this._students.push(student);
   }

   @Log('Professor added')
   addProfessor(professor: Professor): void {
      this._professors.push(professor);
   }

   @Log('Course added')
   addCourse(course: Course): void {
      this._courses.push(course);
   }

   @Log('Student removed')
   removeStudent(studentId: string): void {
      this._students = this._students.filter(
         (student) => student.studentId !== studentId
      );
   }

   @Log('Professor removed')
   removeProfessor(name: string): void {
      this._professors = this._professors.filter(
         (professor) => professor.name !== name
      );
   }

   @Log('Course removed')
   removeCourse(id: string): void {
      this._courses = this._courses.filter((course) => course.id !== id);
   }

   findCourseById(id: string): Course | undefined {
      return this._courses.find((course) => course.id === id);
   }

   @Log('Student enrolled in course')
   enrollStudentInCourse(studentId: string, courseId: string): boolean {
      const student = this._students.find(
         (student) => student.studentId === studentId
      );
      const course = this._courses.find((course) => course.id === courseId);

      if (student && course && course.capacity > 0) {
         student.courses.push(course);
         course.capacity--;
         return true;
      } else {
         return false;
      }
   }

   @Log('Listing students in course')
   listStudentsInCourse(courseId: string): Student[] {
      const studentsInCourse: Student[] = [];

      for (const student of this._students) {
         for (const course of student.courses) {
            if (course.id === courseId) {
               studentsInCourse.push(student);
               break;
            }
         }
      }

      return studentsInCourse;
   }

   findStudentById(studentId: string): Student | undefined {
      return this._students.find((student) => student.studentId === studentId);
   }

   findProfessorByName(name: string): Professor | undefined {
      return this._professors.find((professor) => professor.name === name);
   }

   @Log('Assign professor to course')
   assignProfessorToCourse(professorName: string, courseId: string): boolean {
      const professor = this.findProfessorByName(professorName);
      const course = this.findCourseById(courseId);

      if (professor && course) {
         professor.courses.push(course);
         return true;
      } else {
         return false;
      }
   }

   listCoursesByProfessor(professorName: string): Course[] {
      const professor = this.findProfessorByName(professorName);
      return professor ? professor.courses : [];
   }

   listStaffMembersByDepartment(department: string): StaffMember[] {
      return this._staffMembers.filter(
         (staffMember) => staffMember.department === department
      );
   }

   @Log('Student unenrolled from course')
   unenrollStudentFromCourse(studentId: string, courseId: string): boolean {
      const student = this.findStudentById(studentId);
      const course = this.findCourseById(courseId);

      if (student && course) {
         student.courses = student.courses.filter(
            (course: { id: string; }) => course.id !== courseId
         );
         course.capacity++;
         return true;
      } else {
         return false;
      }
   }

   @Log('Staff member added')
   addStaffMember(staffMember: StaffMember): void {
      this._staffMembers.push(staffMember);
   }

   @Log('QualityControl added')
   addQualityControl(QualityControl: QualityControl): void {
      this._QualityControls.push(QualityControl);
   }

   @Log('Staff member removed')
   removeStaffMember(name: string): void {
      this._staffMembers = this._staffMembers.filter(
         (staffMember) => staffMember.name !== name
      );
   }

   @Log('QualityControl removed')
   removeQualityControl(name: string): void {
      this._QualityControls = this._QualityControls.filter(
         (QualityControl) => QualityControl.name !== name
      );
   }

   @Log('Department added')
   addDepartment(department: Department): void {
      this._departments.push(department);
   }

   @Log('Department removed')
   removeDepartment(name: string): void {
      this._departments = this._departments.filter(
         (department) => department.name !== name
      );
   }

   findDepartmentByName(name: string): Department | undefined {
      return this._departments.find((department) => department.name === name);
   }

   @Log('Assign staff member to department')
   assignStaffMemberToDepartment(
      staffMemberName: string,
      departmentName: string
   ): boolean {
      const staffMember = this._staffMembers.find(
         (staff) => staff.name === staffMemberName
      );
      const department = this.findDepartmentByName(departmentName);

      if (staffMember && department) {
         staffMember.department = departmentName;
         department.addStaffMember(staffMember);
         return true;
      } else {
         return false;
      }
   }

   @Log('Assign professor to department')
   assignProfessorToDepartment(
      professorName: string,
      departmentName: string
   ): boolean {
      const professor = this._professors.find(
         (prof) => prof.name === professorName
      );
      const department = this.findDepartmentByName(departmentName);

      if (professor && department) {
         professor.faculty = departmentName as Faculty;
         department.addProfessor(professor);
         return true;
      } else {
         return false;
      }
   }

   @Log('Assign course to department')
   assignCourseToDepartment(courseId: string, departmentName: string): boolean {
      const course = this._courses.find((course) => course.id === courseId);
      const department = this.findDepartmentByName(departmentName);

      if (course && department) {
         course.faculty = departmentName as Faculty;
         department.addCourse(course);
         return true;
      } else {
         return false;
      }
   }

   listCoursesByDepartment(departmentName: string): Course[] {
      const department = this.findDepartmentByName(departmentName);
      return department ? department.courses : [];
   }
}


export { University}