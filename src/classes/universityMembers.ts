import { Course } from "../interfaces/interfaces";
import { Faculty } from "../enums/enums";

abstract class UniversityMember {
   constructor(public name: string, public age: number) {}

   abstract getMemberType(): string;
}

class StudentMember extends UniversityMember {
   constructor(
      name: string,
      age: number,
      public studentId: string,
      public courses: Course[]
   ) {
      super(name, age);
   }

   getMemberType(): string {
      return 'Student';
   }
}

class ProfessorMember extends UniversityMember {
   constructor(
      name: string,
      age: number,
      public faculty: Faculty,
      public courses: Course[]
   ) {
      super(name, age);
   }

   getMemberType(): string {
      return 'Professor';
   }
}

class StaffMember extends UniversityMember {
   constructor(
      name: string,
      age: number,
      public Role: string,
      public department: string
   ) {
      super(name, age);
   }

   getMemberType(): string {
      return 'StaffMember';
   }
}

class QualityControlMember extends UniversityMember {
   constructor(
      name: string,
      age: number,
      public title: string,
      public departments: string[]
   ) {
      super(name, age);
   }

   getMemberType(): string {
      return 'QualityControl';
   }
}

function isStudent(member: UniversityMember): member is StudentMember {
   return (member as StudentMember).studentId !== undefined;
}

function isProfessor(member: UniversityMember): member is ProfessorMember {
   return (member as ProfessorMember).faculty !== undefined;
}

function isStaff(member: UniversityMember): member is StaffMember {
   return (member as StaffMember).department !== undefined;
}

function isQualityMember(
   member: UniversityMember
): member is QualityControlMember {
   return (member as QualityControlMember).title !== undefined;
}

export { StudentMember, ProfessorMember, QualityControlMember, StaffMember, isStudent, isProfessor, isQualityMember, isStaff };

