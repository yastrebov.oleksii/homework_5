// Department Class
import { StaffMember } from './universityMembers';
import { Professor } from '../types/types';
import { Course } from '../interfaces/interfaces';

class Department {
   private _staffMembers: StaffMember[] = [];
   private _professors: Professor[] = [];
   private _courses: Course[] = [];

   constructor(public name: string) {}

   get staffMembers(): StaffMember[] {
      return this._staffMembers;
   }

   get professors(): Professor[] {
      return this._professors;
   }

   get courses(): Course[] {
      return this._courses;
   }

   addStaffMember(staffMember: StaffMember): void {
      this._staffMembers.push(staffMember);
   }

   addProfessor(professor: Professor): void {
      this._professors.push(professor);
   }

   addCourse(course: Course): void {
      this._courses.push(course);
   }

   removeStaffMember(name: string): void {
      this._staffMembers = this._staffMembers.filter(
         (staffMember) => staffMember.name !== name
      );
   }

   removeProfessor(name: string): void {
      this._professors = this._professors.filter(
         (professor) => professor.name !== name
      );
   }

   removeCourse(id: string): void {
      this._courses = this._courses.filter((course) => course.id !== id);
   }
}

export { Department };
