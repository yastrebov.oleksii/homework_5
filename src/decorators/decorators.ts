function Log(message: string): MethodDecorator {
   return function (
      target: Object,
      propertyKey: string | symbol,
      descriptor: PropertyDescriptor
   ): void {
      const originalMethod = descriptor.value;

      descriptor.value = function (...args: any[]): any {
         console.log(`${message}: ${JSON.stringify(args)}`);
         return originalMethod.apply(this, args);
      };
   };
}

function LogGetter(
   target: Object,
   propertyKey: string | symbol,
   descriptor: PropertyDescriptor
): void {
   const originalGetter = descriptor.get;

   descriptor.get = function (this: any): any {
      console.log(`Accessing getter: ${String(propertyKey)}`);
      return originalGetter?.apply(this);
   };
}

function LogSetter(
   target: Object,
   propertyKey: string | symbol,
   descriptor: PropertyDescriptor
): void {
   const originalSetter = descriptor.set;

   descriptor.set = function (this: any, value: any): void {
      console.log(
         `Setting setter: ${String(propertyKey)} to ${JSON.stringify(value)}`
      );
      originalSetter?.apply(this, [value]);
   };
}

function LogClass(target: Function): void {
   console.log(`Class created: ${target.name}`);
}

export { Log, LogClass, LogSetter, LogGetter}